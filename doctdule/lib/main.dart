import 'dart:async';

import 'package:doctdule/pages/mainpage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './services/countrypicker.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 5), () => {
        Navigator.pushAndRemoveUntil(
      context, 
      MaterialPageRoute(
        builder: (context) => MainPage()
      ), 
     ModalRoute.withName("/Home")
    ),
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return Scaffold(
      backgroundColor: Colors.blue,
    );
  }
}
