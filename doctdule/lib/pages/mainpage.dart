import 'package:doctdule/contants.dart';
import 'package:doctdule/services/countrypicker.dart';
import 'package:doctdule/widgets/appbarlocationtextfield.dart';
import 'package:doctdule/widgets/mainpagebottomnavigationbar.dart';
import 'package:doctdule/widgets/phonenumbertextfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MainPage extends StatefulWidget {
  @override
  _MainrPageState createState() => _MainrPageState();
}

class _MainrPageState extends State<MainPage> {
  String _locationPinImage = "assest/location-pin-128.png";

  void setLocationImage(String locationPinImage) {
    setState(() {
      _locationPinImage = locationPinImage;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
        theme: Theme.of(context).copyWith(
            // override textfield's icon color when selected
            primaryColor: Colors.indigo,
            cursorColor: Colors.indigo,
            accentColor: Colors.indigo),
        home:  Scaffold(
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1,
                  // decoration: BoxDecoration(
                  //   color:Colors.blue,
                  // ),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.1,
                        height: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 50, left: 10, bottom: 10),
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          _locationPinImage,
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.5,
                        margin: EdgeInsets.only(top: 41),
                        alignment: Alignment.centerLeft,
                        child: TextFormField(
                          enabled: false,
                          initialValue: 'Select your Location',
                          style: TextStyle(fontSize: 17),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.180,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.width * 0.124,
                        margin: EdgeInsets.only(top: 30, left: 15, right: 15),
                        child: TextFormField(
                          enabled: true,
                          style: TextStyle(fontSize: 17),
                          decoration: new InputDecoration(
                              prefixIcon: Icon(Icons.search),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffD3D3D3), width: 1.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xffD3D3D3), width: 1.0),
                              ),
                              hintText:
                                  'Search for the medical stores near you',
                              hintStyle: TextStyle(
                                  color: Color(0xffD3D3D3), fontSize: 15)),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            bottomNavigationBar: MainPageBootomNavigationBar(),
          ),
        );
  }
}
