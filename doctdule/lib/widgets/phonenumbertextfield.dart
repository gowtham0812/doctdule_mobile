import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PhoneNumberTextField extends StatefulWidget {
  @override
  _PhoneNumberTextFieldState createState() => _PhoneNumberTextFieldState();
}

class _PhoneNumberTextFieldState extends State<PhoneNumberTextField> {
  FocusNode _focusNode;
  TextEditingController nameTextEditingController = TextEditingController();
  //var maskTextInputFormatter = MaskTextInputFormatter(mask: "+# (###) ###-##-##", filter: { "#": RegExp(r'[0-9]') });
  String _labelText;
  //String _initailvalue = "+91";

  @override
  void initState() {
    super.initState();
    nameTextEditingController.addListener(_hasStartedTyping);
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _hasStartedTyping() {
    setState(() {
      if (nameTextEditingController.text.isNotEmpty) {
        _labelText = '10 digit mobile number';
      } else {
        _labelText = null;
      }
    });
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.phone,
                autofocus: false,
                textAlign: TextAlign.start,
                onChanged: (v) {
                  setState(() {
                    if (v.isNotEmpty) {
                      _labelText = '10 digit mobile number';
                    } else {
                      _labelText = null;
                    }
                  });
                },
                textInputAction: TextInputAction.newline,
                controller: nameTextEditingController,
                style: TextStyle(
                    color: Colors.black87,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(10),
                ],
                decoration: InputDecoration(
                  //contentPadding: EdgeInsets.symmetric(vertical: 10),
                  labelText: _labelText,
                  hintText: '10 digit mobile number',
                  hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                  labelStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          )),
    );
  }
}
