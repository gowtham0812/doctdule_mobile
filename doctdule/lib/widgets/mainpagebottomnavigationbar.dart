import 'package:doctdule/pages/mainpage.dart';
import 'package:doctdule/pages/settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:titled_navigation_bar/titled_navigation_bar.dart';

class MainPageBootomNavigationBar extends StatefulWidget {
  @override
  _MainPageBootomNavigationBarState createState() =>
      _MainPageBootomNavigationBarState();
}

class _MainPageBootomNavigationBarState
    extends State<MainPageBootomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.070,
      child: TitledBottomNavigationBar(
          currentIndex: 0,
          activeColor: Colors.black,
          inactiveColor: Color(0xffD3D3D3), // Use this to update the Bar giving a position
          onTap: (index) {
            print("Selected Index: $index");
          },
          items: [
            TitledNavigationBarItem(title: Text('Orders'), icon: MdiIcons.shoppingOutline),
            TitledNavigationBarItem(title: Text('Offers'), icon: MdiIcons.offer),
            TitledNavigationBarItem(
                title: Text('History'), icon: MdiIcons.history),
            TitledNavigationBarItem(
                title: Text('Profile'), icon: Icons.person_outline),
          ]),
      // child: new TabBar(
      //   tabs: [
      //     Tab(
      //       icon: new Icon(MdiIcons.shopping),
      //       child: Text(
      //         'order',
      //         style: TextStyle(fontFamily: 'OpenSans'),
      //       ),
      //     ),
      //     Tab(
      //       icon: new Icon(MdiIcons.offer),
      //       child: Text(
      //         'offers',
      //         style: TextStyle(fontFamily: 'OpenSans'),
      //       ),
      //     ),
      //     Tab(
      //       icon: new Icon(mdi),
      //       child: Text(
      //         'history',
      //         style: TextStyle(fontFamily: 'OpenSans'),
      //       ),
      //     ),
      //     Tab(
      //       icon: new Icon(Icons.home),
      //       child: Text(
      //         'profile',
      //         style: TextStyle(fontFamily: 'OpenSans'),
      //       ),
      //     ),
      //   ],
      //   labelColor: Colors.black,
      //   unselectedLabelColor: Color(0xffD3D3D3),
      //   indicator: BoxDecoration(
      //     border: Border(
      //       top: BorderSide(color: Theme.of(context).accentColor, width: 3.0),
      //     ),
      //   ),
      //   indicatorSize: TabBarIndicatorSize.tab,
      //   indicatorPadding: EdgeInsets.all(5.0),
      //   indicatorColor: Colors.red,
      // ),
    );
  }
}
