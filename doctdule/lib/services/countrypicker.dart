import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:flutter_country_picker/flutter_country_picker.dart';

class CountryPickerClass extends StatefulWidget {
  @override
  _CountryPickerClassState createState() => _CountryPickerClassState();
}

class _CountryPickerClassState extends State<CountryPickerClass> {
  //Country _selected;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        enabled: false,
        style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500,color: Colors.indigo),
        initialValue: "+91",
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly,
          LengthLimitingTextInputFormatter(3),
        ],
        // Only numbers can be entered
        decoration: InputDecoration(
          border: InputBorder.none,
        ),
      ),
    );
  }
}
